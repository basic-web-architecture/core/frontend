# Stage 1
FROM node:16.10 as buildstep
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run prod-build

# Stage 2
FROM nginx:1.17.1-alpine
ARG backend
ENV backend=$backend
COPY --from=buildstep /app/dist/frontend /usr/share/nginx/html
RUN mkdir /etc/nginx/templates
COPY nginx.conf.template /etc/nginx/templates
COPY entrypoint.sh /etc/nginx/entrypoint.sh
EXPOSE 80 
ENTRYPOINT ["sh","/etc/nginx/entrypoint.sh"]