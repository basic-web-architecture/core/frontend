Create a .npmrc from the template file and populate the created file with the values.
If you are running from windows machine, do the EOL coversion for entrypoint.sh before proceeding.
To run in local mode, run `npm install`, then, `ng serve --open`. Frontend will be up in http://localhost:4200
To run docker container in standalone mode, run the commands `npm run image-build` and `npm run image-run`. Frontend will be up in http://localhost

To push the docker image to AWS repo, run the commands `npm run image-build`, `npm run image-tag` and `npm run image-push`. For pushing, authorization token should be active. 
For authorizing with ECR private repositories in AWS, run the below command:
$aws ecr get-login-password --region <REGION> | docker login --username AWS --password-stdin <PRIVATE_REPO>
For ECR public repo, the command will be:
$aws ecr-public get-login-password --region <REGION> | docker login --username AWS --password-stdin <PUBLIC_REPO>



