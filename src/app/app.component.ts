import { Component , OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  implements OnInit{
  constructor(private http: HttpClient) {}
  title = 'frontend';
   desc='';
  ngOnInit() {
		// API Call
		let headers = new HttpHeaders({
		
		});
		this.http
			.get<any>('/alerts', {
				headers: headers
			})
			.subscribe(data => {
				this.desc=data.description
			});
	}
}
