#!/bin/sh
set -x
echo IP address of backend is : ${backend}
sed -i 's~set $backendurl "$backend";~set $backendurl '$backend';~' /etc/nginx/templates/nginx.conf.template
envsubst "$backendurl" < "/etc/nginx/templates/nginx.conf.template" > "/etc/nginx/conf.d/default.conf"
exec nginx -g "daemon off;" $@